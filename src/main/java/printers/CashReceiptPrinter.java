package printers;

import models.CashReceipt;

public interface CashReceiptPrinter {

    void print(CashReceipt cashReceipt);
}
