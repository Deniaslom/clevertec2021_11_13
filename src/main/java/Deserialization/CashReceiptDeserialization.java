package Deserialization;

import models.CashReceipt;

public interface CashReceiptDeserialization {
    String deserialize(CashReceipt cashReceipt);
}
